import type { NextPage } from "next";
import Head from "next/head";

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Eka</title>
      </Head>
    </div>
  );
};

export default Home;
